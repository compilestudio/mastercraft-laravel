<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IngresoMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingreso', function(Blueprint $table)
		{
			$table->increments('id');
			$table->float('price');
			$table->integer('quantity');
			$table->date('invoice_date');
			$table->integer('no_invoice');
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('product');
			$table->integer('provider_id')->unsigned();
			$table->foreign('provider_id')->references('id')->on('provider');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('user');
			$table->integer('encabezado_id')->unsigned();
			$table->foreign('encabezado_id')->references('id')->on('encabezado_ingreso');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingreso');
	}

}

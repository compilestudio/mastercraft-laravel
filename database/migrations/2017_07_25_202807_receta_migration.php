<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecetaMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('base_id')->unsigned();
			$table->foreign('base_id')->references('id')->on('product');
			$table->string('codigo');
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('product');
			$table->integer('cantidad');
			$table->string('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receta');
	}

}

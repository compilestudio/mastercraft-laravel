<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('min_unit');
			$table->float('min_stock', 8,2);
			$table->float('price', 8,2);
			$table->float('cost', 8,2);
			$table->float('merma', 8,2);
			$table->text('observaciones');
			$table->integer('unit_id')->unsigned();
			$table->foreign('unit_id')->references('id')->on('unit');
			$table->integer('unit_purchase_id')->unsigned();
			$table->foreign('unit_purchase_id')->references('id')->on('unit_purchase');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}

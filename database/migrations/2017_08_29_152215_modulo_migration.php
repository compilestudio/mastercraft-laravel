<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuloMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modulo', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('codigo');
			$table->string('codigo_pieza');
			$table->string('descripcion', 1000);
			$table->integer('cantidad');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modulo');
	}

}

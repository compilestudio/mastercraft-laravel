<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CotizacionMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cotizacion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quantity');
			$table->integer('width');
			$table->integer('height');
			$table->float('price', 8,2);
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('product');
			$table->integer('encabezado_id')->unsigned();
			$table->foreign('encabezado_id')->references('id')->on('encabezado');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cotizacion');
	}

}

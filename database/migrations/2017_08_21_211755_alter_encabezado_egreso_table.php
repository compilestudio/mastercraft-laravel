<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEncabezadoEgresoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('encabezado_egreso', function(Blueprint $table)
		{
			$table->string('comentario');
			$table->dropColumn('encabezado_ingreso_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('encabezado_egreso', function(Blueprint $table)
		{
			
		});
	}

}

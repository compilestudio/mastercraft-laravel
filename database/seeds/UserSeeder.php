<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Faker\Factory as Faker;

class UserSeeder extends Seeder{

	public function run()
	{
		$faker = Faker::create();
		for ($i=0; $i < 3; $i++)
		{
			User::create
			([
				'name' => $faker->word(),
				'lastname' => $faker->word(),
				'email' => $faker->word(),
				'password' => $faker->word(),
				'picture' => $faker->word()
			]);
		}
	}
}
<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteContacto extends Model{
	protected $table = "contacto_cliente";
	protected $primaryKey = "id";
	protected $fillable = ['cliente_id', 'name', 'position', 'phone', 'email', 'observations'];

	public function cliente(){
		return $this->hasMany('App\Cliente');
	}
}
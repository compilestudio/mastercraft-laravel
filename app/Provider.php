<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model{
	protected $table = "provider";
	protected $primaryKey = "id";
	protected $fillable = ['name', 'nit', 'phone', 'address', 'email', 'credit_day'];

}
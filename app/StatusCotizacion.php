<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusCotizacion extends Model{
	protected $table = "status_cotizacion";
	protected $primaryKey = "id";
	protected $fillable = ['name'];

	public function products(){
		return $this->hasMany('Apps\Product');
	}

	public function baseProduct(){
		return $this->hasMany('Apps\Product');
	}
}
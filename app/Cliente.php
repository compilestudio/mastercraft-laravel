<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model{
	protected $table = "cliente";
	protected $primaryKey = "id";
	protected $fillable = ['name', 'nit', 'email', 'address', 'phone'];

	public function cliente(){
		return $this->belongsTo('App\ClienteContacto');
	}

	public function encabezado(){
		return $this->belongsTo('App\Encabezado');
	}
}
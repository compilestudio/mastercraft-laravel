<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitPurchase extends Model{
	protected $table = "unit_purchase";
	protected $primaryKey = "id";
	protected $fillable = ['unit'];

	public function products(){
		return $this->belongsTo('Apps\Product');
	}
}
<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuloType extends Model {

	protected $table = "module_type";
	protected $primaryKey = "id";
	protected $fillable = ['name'];

}

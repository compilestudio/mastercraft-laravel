<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EncabezadoEgreso extends Model {

	protected $table = "encabezado_egreso";
	protected $primaryKey = "id";
	protected $fillable = ['comentario'];
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabricacion extends Model{
	protected $table = "fabricacion";
	protected $primaryKey = "id";
	protected $fillable = ['name', 'quantity', 'type'];
}
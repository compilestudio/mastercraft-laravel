<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model{
	protected $table = "receta";
	protected $primaryKey = "id";
	protected $fillable = ['base_id', 'product_id', 'type', 'cantidad', 'codigo'];

	public function products(){
		return $this->hasMany('Apps\Product');
	}

	public function baseProduct(){
		return $this->hasMany('Apps\Product');
	}
}
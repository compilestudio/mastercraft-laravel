<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model{
	protected $table = "inventario";
	protected $primaryKey = "id";
	protected $fillable = ['product_id', 'quantity'];

	public function products(){
		return $this->hasMany('App\Product');
	}
}
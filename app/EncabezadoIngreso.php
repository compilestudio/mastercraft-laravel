<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EncabezadoIngreso extends Model {

	protected $table = "encabezado_ingreso";
	protected $primaryKey = "id";
	protected $fillable = ['total'];

	public function cliente(){
		return $this->hasMany('App\Cliente');
	}

	public function ingreso(){
		return $this->belongsTo('App\Ingreso');
	}

}

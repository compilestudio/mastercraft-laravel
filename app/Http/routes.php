<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'UserController@index');
Route::get('pdf', 'PdfController@invoice');

Route::group(['middleware' => 'cors'], function()
{
	Route::resource('/units', 'UnitController@showAll');
	Route::resource('/auth', 'UserController@login');
	Route::resource('/unitdelete', 'UnitController@dropUnit');
	Route::resource('/addunit', 'UnitController@addUnit');
	Route::resource('/showunit', 'UnitController@showUnit');
	Route::resource('/editunit', 'UnitController@editUnit');
	
	Route::resource('/products', 'ProductController@showAll');
	Route::resource('/product-delete', 'ProductController@destroy');
	Route::resource('/product-add', 'ProductController@create');
	Route::resource('/product-unit', 'ProductController@showUnit');
	Route::resource('/find-product', 'ProductController@show');
	Route::resource('/edit-product', 'ProductController@update');

	Route::resource('/providers', 'ProviderController@showAll');
	Route::resource('/add-providers', 'ProviderController@create');
	Route::resource('/delete-providers', 'ProviderController@destroy');
	Route::resource('/show-provider', 'ProviderController@show');
	Route::resource('/edit-provider', 'ProviderController@update');

	//Ingresos
	Route::resource('/ingresos', 'IngresosController@index');
	Route::resource('/add-ingresos', 'IngresosController@create');
	Route::resource('/edit-ingresos', 'IngresosController@update');
	Route::resource('/total-ingreso', 'IngresosController@showTotal');
	Route::resource('/show-detail-ingreso', 'IngresosController@showDetail');
	Route::resource('/find-ingreso', 'IngresosController@findRecord');
	Route::resource('/get-encabezados', 'IngresosController@getEncabezados');
	Route::resource('/get-products-ingreso', 'IngresosController@show');

	//Egresos
	Route::resource('/create-egreso', 'EgresosController@create');
	Route::resource('/show-egresos', 'EgresosController@showAll');
	Route::resource('/edit-egreso', 'EgresosController@edit');
	Route::resource('/destroy-egreso', 'EgresosController@destroy');
	Route::resource('/show-encabezados-egreso', 'EgresosController@showEncabezado');
	Route::resource('/show-detail-egreso', 'EgresosController@showDetail');
	Route::resource('/show-kardex', 'EgresosController@showKardex');

	//User
	Route::resource('/users', 'UserController@showAll');
	Route::resource('/find-user', 'UserController@show');
	Route::resource('/create-user', 'UserController@create');
	Route::resource('/delete-user', 'UserController@destroy');
	Route::resource('/update-user', 'UserController@update');
	Route::resource('/update-profile', 'UserController@profileEdit');

	// Inventary
	Route::resource('/show-inventory', 'InventoryController@showAll');

	// Kardex
	Route::resource('/kardex', 'InventoryController@kardex');

	//Fabricacion
	Route::resource('/show-fabricacion', 'FabricacionController@show');
	Route::resource('/add-fabricacion', 'FabricacionController@create');
	Route::resource('/update-fabricacion', 'FabricacionController@update');
	Route::resource('/find-fabricacion', 'FabricacionController@find');
	Route::resource('/delete-fabricacion', 'FabricacionController@destroy');

	//UnitPurchase
	Route::resource('/show-unit-purchase', 'UnitPurchaseController@show');
	Route::resource('/create-unit-purchase', 'UnitPurchaseController@create');
	Route::resource('/update-unit-purchase', 'UnitPurchaseController@edit');
	Route::resource('/delete-unit-purchase', 'UnitPurchaseController@destroy');
	Route::resource('/find-unit-purchase', 'UnitPurchaseController@find');

	//Receta
	Route::resource('/show-receta', 'RecetaController@show');
	Route::resource('/add-receta', 'RecetaController@create');
	Route::resource('/delete-receta', 'RecetaController@destroy');
	Route::resource('/find-receta', 'RecetaController@findRecord');
	Route::resource('/update-receta', 'RecetaController@update');
	Route::resource('/copy-receta', 'RecetaController@showCopy');
	Route::resource('/find-receta-code', 'RecetaController@findByCode');
	Route::resource('/show-receta-code', 'RecetaController@showByCode');
	Route::resource('/get-codes', 'RecetaController@getCodes');
	Route::resource('/get-instalacion', 'RecetaController@getRecetaInstalacion');
	Route::resource('/show-piezas', 'RecetaController@getOnlyCode');

	//Cotizacion
	Route::resource('/create-cotizacion', 'CotizacionController@create');
	Route::resource('/update-cotizacion', 'CotizacionController@update');
	Route::resource('/show-cotizacion', 'CotizacionController@show');
	Route::resource('/delete-cotizacion', 'CotizacionController@destroy');
	Route::resource('/find-cotizacion', 'CotizacionController@find');
	Route::resource('/show-base', 'CotizacionController@showBase');
	Route::resource('/show-detail-cotizacion', 'CotizacionController@showEncabezado');
	Route::resource('/get-cliente', 'CotizacionController@getCliente');

	//Cliente
	Route::resource('/show-clientes', 'ClienteController@show');
	Route::resource('/create-cliente', 'ClienteController@create');
	Route::resource('/update-cliente', 'ClienteController@edit');
	Route::resource('/delete-cliente', 'ClienteController@destroy');
	Route::resource('/find-cliente', 'ClienteController@find');

	//ContactoCliente
	Route::resource('/show-contactos', 'ClienteContactoController@show');
	Route::resource('/delete-contacto', 'ClienteContactoController@destroy');
	Route::resource('/create-contacto', 'ClienteContactoController@create');
	Route::resource('/update-contacto', 'ClienteContactoController@edit');
	Route::resource('/find-contacto', 'ClienteContactoController@find');

	//Encabezado
	Route::resource('/show-encabezado', 'EncabezadoController@show');
	Route::resource('/create-encabezado', 'EncabezadoController@create');
	Route::resource('/show-last-encabezado', 'EncabezadoController@lastRecord');
	Route::resource('/update-status', 'EncabezadoController@updateStatus');

	//StatusCotizacion
	Route::resource('/show-status', 'StatusCotizacionController@show');

	//Modulos
	Route::resource('/show-modulos', 'ModuloController@index');
	Route::resource('/show-modulos-by-code', 'ModuloController@searchByCode');
	Route::resource('/create-modulo', 'ModuloController@create');

	//TypeModules
	Route::resource('/show-type-modules', 'ModuloTypeController@show');


});
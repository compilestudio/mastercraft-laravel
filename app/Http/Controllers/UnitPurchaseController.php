<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\UnitPurchase;

class UnitPurchaseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$name = $request->input('name');
		UnitPurchase::create(['unit' => $name]);

		return response()->json(['status' => true, 'msj' => 'Se agregó correctamente.']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$all = UnitPurchase::all();

		return response()->json(['Unit' => $all]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');

		$unit_purchase = UnitPurchase::find($id);

		if($unit_purchase){
			$unit_purchase->unit = $name;
			$unit_purchase->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function find(Request $request)
	{
		$id = $request->input('id');

		$unit_purchase = UnitPurchase::find($id);

		if($unit_purchase){
			return response()->json(['status' => true, 'Unit' => $unit_purchase]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');
		$unit_purchase = UnitPurchase::find($id);
		$unit_purchase->delete();

		return response()->json(['status' => true]);
	}

}

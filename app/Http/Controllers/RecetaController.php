<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Receta;
use App\Product;

use Illuminate\Support\Facades\DB;

class RecetaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$base_id = $request->input('base_id');
		$product_id = $request->input('product_id');
		$type = $request->input('type');
		$quantity = $request->input('quantity');
		$codigo = $request->input('codigo');

		$last_receta = DB::select('SELECT * FROM product ORDER BY id DESC LIMIT 1');
		$last_id = $last_receta[0]->id;

		$array_create = ['base_id' => $last_id, 'product_id' => $product_id, 'type' => $type, 'cantidad' => $quantity, 'codigo' => $codigo];
		Receta::create($array_create);

		return response()->json(['status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$id = $request->input('id');

		$receta = DB::select("SELECT product.id as 'product_id', receta.cantidad, receta.type, receta.id, product.name, receta.codigo  FROM `receta` INNER JOIN product ON receta.product_id = product.id WHERE codigo = '".$code."'");
		
		if($receta){
			return response()->json(['recetas' => $receta, 'status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$product_id = $request->input('product_id');
		$type = $request->input('type');
		$quantity = $request->input('quantity');

		$findRecord = Receta::find($id);

		if($findRecord){
			$findRecord->product_id = $product_id;
			$findRecord->type = $type;
			$findRecord->cantidad = $quantity;
			$findRecord->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$data_find = Receta::find($id);

		if($data_find){
			$data_find->delete();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function findRecord(Request $request){
		$id = $request->input('id');

		$record = Receta::find($id);

		if($record){
			return response()->json(['record' => $record, 'status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function showCopy(Request $request){
		$id = $request->input('id');

		$record = DB::select('select * from receta where base_id = :base_id', ['base_id' => $id]);

		if($record){
			return response()->json(['record' => $record, 'status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function findByCode(Request $request){
		$code = $request->input('code');

		$record = DB::select("SELECT product.id as 'product_id', receta.cantidad, receta.type, receta.id, product.name, receta.codigo  FROM `receta` INNER JOIN product ON receta.product_id = product.id WHERE codigo = '".$code."'");
		if($record){
			return response()->json(['record' => $record, 'status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function showByCode(Request $request){
		$code = $request->input('code');

		$query = "SELECT * FROM receta WHERE codigo='".$code."'";
		$receta = DB::select($query);

		return response()->json(['receta' => $receta]);
	}

	public function getCodes(){
		$query = "SELECT DISTINCT codigo FROM receta";

		$codes = DB::select($query);
		return response()->json(['codes' => $codes]);
	}

	public function getOnlyCode(){
		$query = "SELECT * FROM receta GROUP BY codigo";
		$record = DB::select($query);

		return response()->json(['data' => $record]);
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Provider;

class ProviderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$name = $request->input('name');
		$address = $request->input('address');
		$email = $request->input('email');
		$nit = $request->input('nit');
		$phone = $request->input('phone');
		$credit_day = $request->input('credit_day');

		$newProvider = ['name' => $name, 'address' => $address, 'email' => $email, 'nit' => $nit, 'phone' => $phone, 'credit_day' => $credit_day];
		Provider::create($newProvider);

		return response()->json(['response' => true, 'mensaje' => 'Se ha agregado correctamente el registro.']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$id = $request->input('id');
		$provider = Provider::find($id);

		if($provider)
		{
			return response()->json(['response' => true, 'provider' => $provider]);
		}else
		{
			return response()->json(['response' => false, 'mensaje' => 'No se ha encontrado resultados.']);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$nit = $request->input('nit');
		$phone = $request->input('phone');
		$address = $request->input('address');
		$email = $request->input('email');
		$credit_day = $request->input('credit_day');

		$provider = Provider::find($id);

		if($provider){
			$provider->name = $name;
			$provider->nit = $nit;
			$provider->phone = $phone;
			$provider->address = $address;
			$provider->email = $email;
			$provider->credit_day = $credit_day;
			$provider->save();

			return response()->json(['status' => true, 'mensaje' => 'Se ha editado registro']);
		}else{
			return response()->json(['status' => false, 'mensaje' => 'No se ha editado registro']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');
		$provider = Provider::find($id);

		if($provider){
			$provider->delete();
			return response()->json(['response' => true, 'mensaje' => 'Se ha eliminado correctamente el registro.']);
		}else{
			return response()->json(['response' => false, 'mensaje' => 'No se ha eliminado correctamente el registro.']);
		}
	}

	public function showAll()
	{
		$providers = Provider::all();
		return response()->json(['providers' => $providers, 'status' => false, 'mensaje' => 'No se ha encontrado registro']);
	}

	

}

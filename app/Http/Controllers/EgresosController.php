<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Egreso;
use App\EncabezadoEgreso;
use App\Inventario;

class EgresosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$egreso = new Egreso();
		$user_id = $request->input('user_id');
		$product_id = $request->input('product_id');
		$quantity = $request->input('quantity');
		$comentario = $request->input('comentario');
		$last_encabezado_id = $request->input('last_encabezado_id');
		
		if($last_encabezado_id == 0){
			$arrayCreate = ['comentario' => $comentario];
			EncabezadoEgreso::create($arrayCreate);

			$query = "SELECT * FROM encabezado_egreso ORDER BY id DESC LIMIT 1";
			$encabezado_egreso = DB::select($query);
			$last_encabezado_id = $encabezado_egreso[0]->id;
		}

		$egreso->quantity = $quantity;
		$egreso->product_id = $product_id;
		$egreso->user_id = $user_id;
		$egreso->comentario = $comentario;
		$egreso->encabezado_egreso_id = $last_encabezado_id;
		$egreso->save();
		
		return response()->json(['response' => true, 'mensaje' => 'Se ha agregado correctamente el registro.', 'last_encabezado_id' => $last_encabezado_id]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		$id = $request->input('id');
		$quantity = $request->input('quantity');
		$user_id = $request->input('user_id');
		$product_id = $request->input('product_id');
		$comentario = $request->input('comentario');

		$egreso = Egreso::find($id);
		$egreso->quantity = $quantity;
		$egreso->user_id = $user_id;
		$egreso->product_id = $product_id;
		$egreso->comentario = $comentario;
		$egreso->save();

		return response()->json(['response' => true, 'egresos' => $egreso]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$egreso = Egreso::find($id);
		$egreso->delete();

		return response()->json(['response' => true, 'message' => 'Se ha eliminado correctamente el registro.']);
	}

	public function showAll(){
		// $egresos = Egreso::all();

		$egresos = DB::table('egreso')
            ->join('product', 'egreso.product_id', '=', 'product.id')
            ->join('user', 'egreso.user_id', '=', 'user.id')
            ->select('egreso.id', 'egreso.quantity', 'product.name as product_name', 'egreso.user_id', 'user.name as user_name', 'egreso.product_id', 'egreso.comentario')
            ->get();
		return response()->json(['response' => true, 'egresos' => $egresos]);
	}

	public function showEncabezado(){
		$encabezados = EncabezadoEgreso::all();

		return response()->json(['data' => $encabezados]);
	}

	public function showDetail(Request $request){
		$encabezado_egreso_id = $request->input('encabezado_id');

		$query = "SELECT egreso.id, egreso.quantity, product.name as 'product_name', egreso.user_id, user.name as 'user_name', egreso.product_id, egreso.comentario, egreso.encabezado_egreso_id FROM `egreso` INNER JOIN product ON egreso.product_id = product.id INNER JOIN user ON egreso.user_id = user.id WHERE egreso.encabezado_egreso_id = ".$encabezado_egreso_id;
		$data = DB::select($query);

		return response()->json(['data' => $data]);
	}

	public function showKardex(Request $request){
		$product_id = $request->input('product_id');

		$query = "SELECT * FROM ((SELECT created_at, '' comentario, '' egreso, quantity ingreso FROM `ingreso`  WHERE product_id = ".$product_id.") UNION (SELECT created_at, comentario, quantity egreso, '' ingreso FROM `egreso`  WHERE product_id = ".$product_id.")) a ORDER BY created_at DESC";

		$data = DB::select($query);

		return response()->json(['data' => $data]);
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Cotizacion;
use App\Receta;
use App\Encabezado;

class CotizacionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$quantity = $request->input('quantity');
		$width = $request->input('width');
		$height = $request->input('height');
		$price = $request->input('price');
		$encabezado_id = $request->input('encabezado_id');
		$codigo = $request->input('codigo');
		$today = date('Y-m-d H:i:s');

		$query_last = "SELECT * FROM product LIMIT 1";
		$result = DB::select($query_last);
		$product_id = $result[0]->id;

		$query = "INSERT INTO `cotizacion` (`quantity`, `width`, `height`, `price`, `product_id`, `encabezado_id`, `created_at`, `updated_at`, `codigo`) VALUES ('".$quantity."', '".$width."', '".$height."', '".$price."', '".$product_id."', '".$encabezado_id."', '".$today."', '2017-07-19 00:00:00', '".$codigo."')";
		$last = DB::select($query);

		return response()->json(['status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$cotizaciones = DB::select("SELECT cotizacion.encabezado_id, cliente.name as 'name_client',  cotizacion.id, cotizacion.quantity, cotizacion.width, cotizacion.height, cotizacion.price, cotizacion.product_id, product.name, encabezado.status_id FROM `cotizacion` INNER JOIN product ON cotizacion.product_id = product.id INNER JOIN encabezado ON cotizacion.encabezado_id = encabezado.id INNER JOIN cliente ON encabezado.cliente_id = cliente.id GROUP BY cotizacion.encabezado_id");

		if($cotizaciones){
			$status = true;
		}else{
			$status = false;
		}

		return response()->json(['cotizaciones' => $cotizaciones, 'status' => $status]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$quantity = $request->input('quantity');
		$price = $request->input('price');
		$width = $request->input('width');
		$height = $request->input('height');
		$codigo = $request->input('codigo');

		$find = Cotizacion::find($id);

		if($find){
			$find->quantity = $quantity;
			
			if($price){
				$find->price = $price;
			}

			$find->width = $width;
			$find->height = $height;
			$find->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$find = Cotizacion::find($id);

		if($find){
			$find->delete();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function find(Request $request){
		$id = $request->input('id');

		$find = Cotizacion::find($id);

		if($find){
			return response()->json(['cotizacion' => $find]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function showBase()
	{
		$base_products = DB::select('SELECT DISTINCT base_id FROM receta');
		$allProducts = array();

		foreach ($base_products as $base) { 
			$product = DB::select("SELECT * FROM product WHERE id = ".$base->base_id);
			array_push($allProducts, $product[0]);
		}

		return response()->json(['base_products' => $allProducts]);

	}

	public function showEncabezado(Request $request){
		$id = $request->input('id');
		$query = 'SELECT cotizacion.encabezado_id, cotizacion.id, cotizacion.quantity, cotizacion.width, cotizacion.height, cotizacion.price, cotizacion.product_id, product.name, encabezado.cliente_id as cliente, cliente.id as "client_id", cliente.name as "client_name", cliente.nit FROM `cotizacion` INNER JOIN product ON cotizacion.product_id = product.id INNER JOIN encabezado ON cotizacion.encabezado_id = encabezado.id INNER JOIN cliente ON encabezado.cliente_id = cliente.id WHERE encabezado_id='.$id;
		$cotizaciones = DB::select($query);

		return response()->json(['cotizaciones' => $cotizaciones]);
	}

	public function getCliente(Request $request){
		$encabezado_id = $request->input('encabezado_id');

		$query_encabezado = "SELECT encabezado.cliente_id, encabezado.id as 'encabezado_id', cliente.name, cliente.id, cliente.nit FROM encabezado INNER JOIN cliente ON encabezado.cliente_id = cliente.id WHERE encabezado.id = ".$encabezado_id;

		$encabezado_data = DB::select($query_encabezado);

		return response()->json(['data' => $encabezado_data]);
	}

	public function getRecetaInstalacion(){
		
	}

}

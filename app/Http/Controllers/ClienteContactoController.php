<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ClienteContacto;

class ClienteContactoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$cliente_id = $request->input('cliente_id');
		$name = $request->input('name');
		$position = $request->input('position');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$observations = $request->input('observations');

		$array_create = ['cliente_id' => $cliente_id, 'name' => $name, 'position' => $position, 'phone' => $phone, 'email' => $email, 'observations' => $observations];

		ClienteContacto::create($array_create);

		return response()->json(['status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$cliente_id = $request->input('cliente_id');
		$all = DB::select('SELECT * FROM contacto_cliente WHERE cliente_id='.$cliente_id);

		return response()->json(['contactos' => $all]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		$id = $request->input('id');
		$cliente_id = $request->input('cliente_id');
		$name = $request->input('name');
		$position = $request->input('position');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$observations = $request->input('observations');

		$find = ClienteContacto::find($id);

		if($find){
			$find->cliente_id = $cliente_id;
			$find->name = $name;
			$find->position = $position;
			$find->phone = $phone;
			$find->email = $email;
			$find->observations = $observations;
			$find->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$find = ClienteContacto::find($id);

		if($find){
			$find->delete();
			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function find(Request $request){
		$id = $request->input('id');

		$find = ClienteContacto::find($id);

		if($find){
			return response()->json(['contacto' => $find]);
		}else{
			return response()->json(['status' => false]);
		}
	}

}

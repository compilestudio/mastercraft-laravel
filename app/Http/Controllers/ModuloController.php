<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Modulo;

class ModuloController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$query = "SELECT id, codigo, descripcion, codigo_pieza, SUM(cantidad) as cantidad, created_at, updated_at FROM modulo GROUP BY codigo";
		$all = DB::select($query);

		return response()->json(['data' => $all]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$codigo = $request->input('codigo');
		$descripcion = $request->input('descripcion');
		$codigo_pieza = $request->input('codigo_pieza');
		$tipo = $request->input('type');
		$cantidad = $request->input('cantidad');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function searchByCode(Request $request){
		$code = $request->input("code");

		$query = "SELECT modulo.id, modulo.codigo, modulo.descripcion, modulo.codigo_pieza, modulo.cantidad, modulo.created_at, module_type.id, module_type.name FROM `modulo` INNER JOIN module_type ON modulo.module_type_id = module_type.id WHERE modulo.codigo = '".$code."'";
		$data = DB::select($query);

		return response()->json(["data" => $data]);
	}

}

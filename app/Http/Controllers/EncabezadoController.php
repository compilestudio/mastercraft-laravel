<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Encabezado;

class EncabezadoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$cliente_id = $request->input('cliente_id');
		$array_create = ['cliente_id' => $cliente_id, 'status_id' => 1];
		Encabezado::create($array_create);

		return response()->json(['status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$all = Encabezado::all();

		return response()->json(['encabezados' => $all]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function lastRecord(){
		$last = DB::select('SELECT * FROM encabezado ORDER BY id DESC LIMIT 1');

		if($last){
			$status = true;
		}else{
			$status = false;
		}
		return response()->json(['last' => $last, 'status' => $status]);
	}

	public function updateStatus(Request $request){
		$id = $request->input('id');
		$status_id = $request->input('status_id');

		$find = Encabezado::find($id);

		if($find){
			$find->status_id = $status_id;
			$find->save();

			return response()->json(["status" => true]);
		}else{
			return response()->json(["status" => false]);
		}
	}

}

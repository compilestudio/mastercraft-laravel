<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Product;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$name = $request->input('name');
		$min_unit = $request->input('min_unit');
		$min_stock = $request->input('min_stock');
		$price = $request->input('price');
		$merma = $request->input('merma');
		$description = $request->input('description');

		$cost = $request->input('cost');
		$unit_purchase_id = $request->input('unit_purchase_id');
		$unit_id = $request->input('unit_id');

		$newProduct = [
			'name' => $name,
			'min_unit' => $min_unit,
			'min_stock' => $min_stock,
			'unit_purchase_id' => $unit_purchase_id,
			'unit_id' => $unit_id,
			'cost' => $cost,
			'price' => $price,
			'merma' => $merma,
			'observaciones' => $description
			];
		Product::create($newProduct);

		// return response()->json(['response' => $newProduct]);
		return response()->json(['response' => true, 'mensaje' => 'Se ha agregado correctamente el registro.']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$id = $request->input('id');
		$product = Product::find($id);

		if($product)
		{
			return response()->json(['response' => true, 'product' => $product]);
		}else
		{
			return response()->json(['response' => false, 'mensaje' => 'No se ha encontrado resultados.']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$min_unit = $request->input('min_unit');
		$min_stock = $request->input('min_stock');
		$price = $request->input('price');
		$merma = $request->input('merma');
		$description = $request->input('description');

		$product = Product::find($id);

		if($product){
			$product->name = $name;
			$product->min_unit = $min_unit;
			$product->min_stock = $min_stock;
			$product->price = $price;
			$product->merma = $merma;
			$product->observaciones = $description;
			$product->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');
		$product = Product::find($id);

		if($product){
			$product->delete();
			return response()->json(['status' => true, 'message' => 'Se ha eliminado el registro.']);
		}else{
			return response()->json(['status' => false, 'message' => 'No se ha eliminado el registro.']);
		}
	}

	public function showAll(){
        $show = DB::select('SELECT * FROM product');

		if(!$show){
			return response()->json(['status' => false, 'message' => 'No se encontraron resultados']);
		}else{
			return response()->json(['status' => true, 'Products' => $show]);
		}
	}

	public function showUnit(Request $request){
		$id = $request->input('id');

		$UnitFind = Product::find($id);

		if(!$UnitFind){
			return response()->json(['status' => false, 'message' => 'No se encontraron resultados']);
		}else{
			return response()->json(['status' => true, 'Unit' => $UnitFind]);
		}
	}

}

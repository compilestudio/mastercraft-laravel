<?php namespace App\Http\Controllers;

use App\MiModelo;

class MiControlador extends Controller {

	function index(){
		$Modelo = new MiModelo();
		$resultado = $Modelo->saludar("Kenny");

		return view("Prueba.index", ["saludo" => $resultado]);
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Inventario;
use App\Ingreso;
use App\Egreso;
use App\Product;

class InventoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function showAll(){
		$show = DB::select("SELECT ingreso.id, product.name as 'product_name', SUM(ingreso.quantity) as 'quantity', ingreso.price, ingreso.no_invoice, ingreso.invoice_date, ingreso.encabezado_id, ingreso.provider_id, ingreso.product_id FROM `ingreso` INNER JOIN product ON ingreso.product_id = product.id GROUP BY ingreso.product_id");

        if($show){
        	$status = true;
        }else{
        	$status = false;
        }
        return response()->json(['inventario' => $show, 'status' => $status]);
	}

	/**
	 *
	 * Kardex
	 * @return Kardex
	 *
	 */
	public function kardex(){
		$product = Product::all();

		$ingreso = DB::select('SELECT id, price, SUM(quantity) as quantity, no_invoice, invoice_date, product_id, user_id FROM `ingreso` GROUP BY product_id');

		$egreso = DB::select('SELECT id, SUM(quantity) as quantity, comentario, product_id, user_id FROM `egreso` GROUP BY product_id');

		return response()->json(['ingreso' => $ingreso, 'egreso' => $egreso, 'product' => $product]);
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Ingreso;
use App\Inventario;
use App\EncabezadoIngreso;

class IngresosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ingresos = DB::select("SELECT ingreso.id, product.name as 'product_name', provider.name as 'provider_name', ingreso.quantity, ingreso.price, DATE_FORMAT(encabezado_ingreso.created_at, '%Y-%m-%d') as 'fecha', user.name as 'user_name', ingreso.encabezado_id FROM ingreso INNER JOIN product ON ingreso.product_id = product.id INNER JOIN provider ON ingreso.provider_id = provider.id INNER JOIN user ON ingreso.user_id = user.id INNER JOIN encabezado_ingreso ON ingreso.encabezado_id = encabezado_ingreso.id GROUP BY ingreso.encabezado_id");

		return response()->json(['ingresos' => $ingresos]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$product_id = $request->input('product_id');
		$price = $request->input('price');
		$quantity = $request->input('quantity');
		$provider_id = $request->input('provider_id');
		$invoice_date = $request->input('invoice_date');
		$no_invoice = $request->input('no_invoice');
		$total = $request->input('total');
		$last_header_request = $request->input('last_header_request');
		$user_id = 1;

		if($last_header_request == 0){
			$array_header = ['total' => $total];
			EncabezadoIngreso::create($array_header);

			$last = DB::select('SELECT * FROM `encabezado_ingreso` ORDER BY id DESC LIMIT 1');
			$last_header_id = $last[0]->id;
		}else{
			$last_header_id = $last_header_request;
		}

		$newIngresos = ['product_id' => $product_id, 'price' => $price, 'quantity' => $quantity, 'provider_id' => $provider_id, 'user_id' => $user_id, 'invoice_date' => $invoice_date, 'no_invoice' => $no_invoice, 'encabezado_id' => $last_header_id];

		Ingreso::create($newIngresos);

		return response()->json(['response' => true, 'mensaje' => 'Se ha agregado correctamente el registro.', 'last_id' => $last_header_id]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$id = $request->input('id');

		$show = DB::table('ingreso')
            ->join('product', 'ingreso.product_id', '=', 'product.id')
            ->join('user', 'ingreso.user_id', '=', 'user.id')
            ->join('provider', 'ingreso.provider_id', '=', 'provider.id')
            ->select('ingreso.id', 'ingreso.price', 'ingreso.quantity', 'product.name as product_name', 'ingreso.user_id', 'user.name as user_name', 'ingreso.provider_id', 'provider.name as provider_name', 'ingreso.product_id')
            ->where('ingreso.id', '=', $id)
            ->get();
        if($show){
        	$status = true;
        }else{
        	$status = false;
        }
        return response()->json(['ingresos' => $show, 'status' => $status]);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$price = $request->input('price');
		$quantity = $request->input('quantity');
		$provider_id = $request->input('provider_id');
		$product_id = $request->input('product_id');
		$no_invoice = $request->input('no_invoice');
		$invoice_date = $request->input('invoice_date');

		$encabezado_id = $request->input('encabezado_id');
		$total = $request->input('total');
		$encabezado = EncabezadoIngreso::find($encabezado_id);

		if($encabezado){
			$encabezado->total = $total;
			$encabezado->save();
		}

		$ingreso = Ingreso::find($id);

		if($ingreso){
			$ingreso->price = $price;
			$ingreso->quantity = $quantity;
			$ingreso->provider_id = $provider_id;
			$ingreso->product_id = $product_id;
			$ingreso->no_invoice = $no_invoice;
			$ingreso->invoice_date = $invoice_date;

			$ingreso->save();

			return response()->json(['message' => 'Se edito el registro correctamente', 'status' => true]);
		}else{
			return response()->json(['message' => 'No se edito el registro correctamente', 'status' => false]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

	public function showAll()
	{
		$show = DB::table('ingreso')
            ->join('product', 'ingreso.product_id', '=', 'product.id')
            ->join('user', 'ingreso.user_id', '=', 'user.id')
            ->join('provider', 'ingreso.provider_id', '=', 'provider.id')
            ->select('ingreso.id', 'ingreso.price', 'ingreso.quantity', 'product.name as product_name', 'ingreso.user_id', 'user.name as user_name', 'ingreso.provider_id', 'provider.name as provider_name', 'ingreso.product_id', 'ingreso.encabezado_id')
            ->get();

        return response()->json(['ingresos' => $show, 'status' => false]);
	}

	public function showTotal(Request $request)
	{
		$id = $request->input('id');

		$ingreso = DB::table('ingreso')->where('product_id', '=', $id)->sum('quantity');
		return response()->json(['total_ingreso' => $ingreso, 'status' => true]);
	}

	public function showDetail(Request $request)
	{
		$encabezado_id = $request->input('encabezado_id');

		$ingresos = DB::select("SELECT ingreso.id, product.name as 'product_name', provider.name as 'provider_name', ingreso.quantity, ingreso.price, user.name as 'user_name', ingreso.encabezado_id FROM ingreso INNER JOIN product ON ingreso.product_id = product.id INNER JOIN provider ON ingreso.provider_id = provider.id INNER JOIN user ON ingreso.user_id = user.id WHERE encabezado_id=".$encabezado_id);

		return response()->json(['ingresos' => $ingresos]);
	}

	public function findRecord(Request $request)
	{
		$id = $request->input('id');

		$record = DB::select("SELECT ingreso.id, product.name as 'product_name', provider.name as 'provider_name', ingreso.quantity, product.id as 'product_id', provider.id as 'provider_id', ingreso.price, user.name as 'user_name', ingreso.encabezado_id, ingreso.no_invoice, ingreso.invoice_date FROM ingreso INNER JOIN product ON ingreso.product_id = product.id INNER JOIN provider ON ingreso.provider_id = provider.id INNER JOIN user ON ingreso.user_id = user.id WHERE ingreso.id=".$id);

		return response()->json(['record' => $record]);
	}

	public function getEncabezados(){
		$record = DB::select("SELECT * FROM encabezado_ingreso");

		return response()->json(['data' => $record]);
	}

}

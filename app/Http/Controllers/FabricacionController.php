<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Fabricacion;

class FabricacionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$name = $request->input('name');
		$quantity = $request->input('quantity');
		$type = $request->input('type');

		$newFabricacion = ['name' => $name, 'quantity' => $quantity, 'type' => $type];
		Fabricacion::create($newFabricacion);

		return response()->json(['status' => true, 'msj' => 'Se agregó correctamente']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$allElements = Fabricacion::all();

		return response()->json(['fabricacion' => $allElements]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$quantity = $request->input('quantity');
		$type = $request->input('type');

		$fabricacion = Fabricacion::find($id);

		if($fabricacion){
			$fabricacion->name = $name;
			$fabricacion->quantity = $quantity;
			$fabricacion->type = $type;
			$fabricacion->save();

			return response()->json(['status' => true, 'msj' => 'Se editó correctamente']);
		}else{
			return response()->json(['status' => false, 'msj' => 'Ocurrió un error']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$fabricacion = Fabricacion::find($id);
		$fabricacion->delete();

		return response()->json(['status' => true, 'msj' => 'Se eliminó correctamente']);
	}

	public function find(Request $request){
		$id = $request->input('id');
		$fabricacion = Fabricacion::find($id);

		return response()->json(['status' => true, 'fabricacion' => $fabricacion]);
	}

}

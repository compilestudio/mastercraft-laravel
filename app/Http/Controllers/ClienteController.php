<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$name = $request->input('name');
		$nit = $request->input('nit');
		$email = $request->input('email');
		$address = $request->input('address');
		$phone = $request->input('phone');

		$array_create = ['name' => $name, 'nit' => $nit, 'email' => $email, 'address' => $address, 'phone' => $phone];
		Cliente::create($array_create);

		return response()->json(['status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$clients = Cliente::all();

		return response()->json(['clients' => $clients]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$nit = $request->input('nit');
		$email = $request->input('email');
		$address = $request->input('address');
		$phone = $request->input('phone');

		$find = Cliente::find($id);

		if($find){
			$find->name = $name;
			$find->nit = $nit;
			$find->email = $email;
			$find->address = $address;
			$find->phone = $phone;
			$find->save();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$find = Cliente::find($id);

		if($find){
			$find->delete();

			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
	}

	public function find(Request $request){
		$id = $request->input('id');

		$find = Cliente::find($id);

		if($find){
			return response()->json(['cliente' => $find]);
		}else{
			return response()->json(['status' => false]);
		}
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Units;

class UnitController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');
		$units = new Units();

		$unit = $units->find($id);
		return response()->json(['Units' => $id]);		
	}

	public function showAll(){
		$units = new Units();

		return response()->json(['Units' => $units->all()]);
	}

	public function dropUnit(Request $request)
	{
		$id = $request->input('id');
		$units = new Units();

		$unit = $units->find($id);

		if($unit){
			$unit->delete();
			return response()->json(['response' => true, 'mensaje' => 'Se ha eliminado correctamente el registro.']);
		}else{
			return response()->json(['response' => false, 'mensaje' => 'No se ha encontrado resultados.']);
		}
	}

	public function addUnit(Request $request){
		$name = $request->input('name');
		Units::create(['unit' => $name]);

		return response()->json(['response' => true, 'mensaje' => 'Se ha agregado correctamente el registro.']);
	}

	public function showUnit(Request $request){
		$id = $request->input('id');
		$unit = Units::find($id);

		if($unit){
			return response()->json(['Unit' => $unit, 'status' => true, 'mensaje' => 'Se ha encontrado registro']);
		}else{
			return response()->json(['Unit' => $unit, 'status' => false, 'mensaje' => 'No se ha encontrado registro']);
		}
	}

	public function editUnit(Request $request){
		$id = $request->input('id');
		$name = $request->input('name');
		$unit = Units::find($id);


		if($unit){
			$unit->unit = $name;
			$unit->save();
			return response()->json(['Unit' => $unit, 'status' => true, 'mensaje' => 'Se ha encontrado registro']);
		}else{
			return response()->json(['Unit' => $unit, 'status' => false, 'mensaje' => 'No se ha encontrado registro']);
		}
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Routing\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;
use Auth;
use Hash;

class UserController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userModel = new User();

		return response()->json(['Users' => $userModel->all()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$path = app_path("..") . '/resources/img/users/';

		$name = $request->input('name');
		$lastname = $request->input('lastname');
		$email = $request->input('email');
		$password = $request->input('password');
		$base64 = $request->input('picture');

		$last_user = DB::select('SELECT * FROM `user` ORDER BY id DESC LIMIT 1');
		if($last_user == ""){
			$newId = $last_user[0]->id + 1;
		}else{
			$newId = 1;
		}


		if($base64 != ""){
			$imgFile = base64_decode(
	            preg_replace('#^data:image/\w+;base64,#i', '', $base64)
	        );

	        file_put_contents($path . $newId . ".jpg", $imgFile);

	        $host= $_SERVER["HTTP_HOST"];
			$url= $_SERVER["REQUEST_URI"];
			$imgDB = "http://" . $host."/RESTApi/resources/img/users/" . $newId . ".jpg" ;

			$arrayCrete = ['name' => $name, 'lastname' => $lastname, 'email' => $email, 'password' => $password, 'picture' => $imgDB];
			User::create($arrayCrete);
		}else{
			$arrayCrete = ['name' => $name, 'lastname' => $lastname, 'email' => $email, 'password' => $password, 'picture' => ''];
			User::create($arrayCrete);
		}

		return response()->json(['message' => 'Se ha creado el nuevo usuario', 'status' => true]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request)
	{
		$id = $request->input('id');
		$user = User::find($id);
		if(!$user){
			return response()->json(['user' => '', 'message' => 'No existe registro', 'status' => false]);
		}else{
			return response()->json(['user' => $user, 'message' => 'Se encontro registro', 'status' => true]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id = $request->input('id');

		$user = User::find($id);
		$user->delete();

		return response()->json(['message' => 'Se elimino correctamente el registro', 'status' => true]);
	}

	public function login(Request $request){
		$email = $request->input('email');
		$password = $request->input('password');
		
		$user = DB::select('select * from user where email = :email and password = :password', ['email' => $email, 'password' => $password]);
		
		if($user){
			return response()->json(['status' => true, 'user' =>  $user]);
		}else{
			return response()->json(['status' => false, 'user' =>  $user]);
		}
	}

	public function showAll(){
		$users = User::all();

		return response()->json(['users' =>  $users]);
	}

	public function profileEdit(Request $request){
		$path = app_path("..") . '/resources/img/users/';

		$id = $request->input('id');
		$name = $request->input('name');
		$lastname = $request->input('lastname');
		$email = $request->input('email');
		$password = $request->input('password');
		$base64 = $request->input('picture');

		$user = User::find($id);

		if($user){
			$user->name = $name;
			$user->lastname = $lastname;
			$user->email = $email;
			
			if($password != ""){
				$user->password = $password;
			}

			if($base64 != ""){
				$imgFile = base64_decode(
		            preg_replace('#^data:image/\w+;base64,#i', '', $base64)
		        );

		        file_put_contents($path . $id . ".jpg", $imgFile);

		        $host= $_SERVER["HTTP_HOST"];
				$url= $_SERVER["REQUEST_URI"];
				$imgDB = "http://" . $host."/RESTApi/resources/img/users/" . $id . ".jpg" ;

				$user->picture = $imgDB;
			}
			
			$user->save();
		}

		return response()->json(['user' => $base64]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$path = app_path("..") . '/resources/img/users/';

		$id = $request->input('id');
		$name = $request->input('name');
		$lastname = $request->input('lastname');
		$email = $request->input('email');
		$password = $request->input('password');
		$base64 = $request->input('picture');

		$user = User::find($id);

		if($user){
			$user->name = $name;
			$user->lastname = $lastname;
			$user->email = $email;
			
			if($password != ""){
				$user->password = $password;
			}

			if($base64 != ""){
				$imgFile = base64_decode(
		            preg_replace('#^data:image/\w+;base64,#i', '', $base64)
		        );

		        file_put_contents($path . $id . ".jpg", $imgFile);

		        $host= $_SERVER["HTTP_HOST"];
				$url= $_SERVER["REQUEST_URI"];
				$imgDB = "http://" . $host."/RESTApi/resources/img/users/" . $id . ".jpg" ;

				$user->picture = $imgDB;
			}
			
			$user->save();

			return response()->json(['status' =>  false, 'message' => 'Se edito el registro correctamente', 'picture' => $base64]);
		}else{
			return response()->json(['status' =>  false, 'message' => 'No se edito el registro']);
		}
	}
}

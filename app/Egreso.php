<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Egreso extends Model{
	protected $table = "egreso";
	protected $primaryKey = "id";
	protected $fillable = ['product_id', 'comentario', 'quantity', 'user_id', 'encabezado_egreso_id'];

	public function products(){
		return $this->hasMany('App\Product');
	}

	public function users(){
		return $this->hasMany('App\User');
	}
}
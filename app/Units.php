<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Units extends Model{
	protected $table = "unit";
	protected $primaryKey = "id";
	protected $fillable = ['unit'];

	public function producto(){
		return $this->belongsTo('App\Product');
	}
}

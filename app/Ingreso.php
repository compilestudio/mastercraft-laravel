<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model{
	protected $table = "ingreso";
	protected $primaryKey = "id";
	protected $fillable = ['product_id', 'price', 'invoice_date', 'no_invoice', 'quantity', 'provider_id', 'user_id', 'encabezado_id'];

	public function products(){
		return $this->hasMany('App\Product');
	}

	public function providers(){
		return $this->hasMany('App\Provider');
	}

	public function users(){
		return $this->hasMany('App\User');
	}

	public function encabezadoIngreso(){
		return $this->hasMany('App\EncabezadoIngreso');
	}
}
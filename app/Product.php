<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
	protected $table = "product";
	protected $primaryKey = "id";
	protected $fillable = ['name', 'min_unit', 'min_stock', 'price', 'cost', 'merma', 'observaciones', 'unit_id', 'unit_purchase_id'];

	public function units(){
		return $this->hasMany('App\Units');
	}

	public function unitPurchase(){
		return $this->hasMany('App\UnitPurchase');
	}

	public function ingresos(){
		return $this->belongsTo('App\Ingresos');
	}

	public function egresos(){
		return $this->belongsTo('App\Egresos');
	}

	public function inventario(){
		return $this->belongsTo('App\Inventario');
	}

	public function receta(){
		return $this->belongsTo('App\Receta');
	}

	public function cotizacion(){
		return $this->belongsTo('App\Cotizacion');
	}
}
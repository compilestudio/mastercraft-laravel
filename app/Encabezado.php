<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Encabezado extends Model{
	protected $table = "encabezado";
	protected $primaryKey = "id";
	protected $fillable = ['cliente_id'];

	public function cliente(){
		return $this->hasMany('App\Cliente');
	}

	public function cotizacion(){
		return $this->belongsTo('App\Cotizacion');
	}
}
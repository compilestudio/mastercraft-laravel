<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model {
	protected $table = "modulo";
	protected $primaryKey = "id";
	protected $fillable = ['descripcion', 'codigo_pieza', 'cantidad', 'codigo'];
}

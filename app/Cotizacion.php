<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model{
	protected $table = "cotizacion";
	protected $primaryKey = "id";
	protected $fillable = ['quantity', 'product_id', 'width', 'height', 'price', 'encabezado_id', 'codigo'];

	public function encabezado(){
		return $this->hasMany('App\Encabezado');
	}
}